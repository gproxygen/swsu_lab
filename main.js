function updateCounter(V){
        $('#counter').html(V.counter);
}

setInterval(function(){
    var obj = {
        x:1
    }
    $.ajax(
        {
            url: "client.php",
            type: "GET",
            data: {update: obj},
            beforeSend: function (data) {
            },
            success: function (data) {
                updateCounter(JSON.parse(data));
            }
        })

}, 2000);

function Increment() {
    var obj = {
        x: 15
    };
    $.ajax(
        {
            url: "client.php",
            type: "POST",
            data: {increment: obj},
            beforeSend: function (data) {
            },
            success: function (data) {
                updateCounter(JSON.parse(data));
            }
        })
}