<?php
error_reporting(E_ERROR);
require_once "client.php";
?>
<!DOCTYPE html>
<html>
<head>
    <title>ЮЗГУ 2017</title>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="main.js" type="text/javascript"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>

<body>
<div class="container m-top">

    <div id='info_message' class="alert alert-success">
        Лабораторная работа № <?php echo $lab_n; ?>
    </div>

    <div class="text-center font-large" id="counter">
        <?php if ($db == nil) {
            printf("Нет доступа к БД");
        } else {
            echo GetStatistics();
        } ?>
    </div>


    <div id='info_message2' class="m-top alert alert-warning">
        <button class="btn btn-success center-block " onclick="Increment()">Выполнить</button>
        <a name="start" id="info_message"></a>
    </div>


</div>


</body>

</html>
